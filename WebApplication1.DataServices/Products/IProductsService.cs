﻿using System.Collections.Generic;

namespace WebApplication1.DataServices.Products
{
    public interface IProductsService
    {
        IEnumerable<Product> GetAllProducts();
        Product GetProductById(int id);
    }
}