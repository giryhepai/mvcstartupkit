﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.DataServices.DataProviders;

namespace WebApplication1.DataServices.Products
{
    public class ProductsService : IProductsService
    {
        private readonly IProductProvider productProvider;

        public ProductsService(IProductProvider productProvider)
        {
            this.productProvider = productProvider;
        }

        public IEnumerable<Product> GetAllProducts()
        {
            return productProvider.GetProducts();
        }

        public Product GetProductById(int id)
        {
            return productProvider.GetProducts().Where(p => p.Id == id).FirstOrDefault();
        }
    }
}