﻿using System.Collections.Generic;
using WebApplication1.DataServices.Products;

namespace WebApplication1.DataServices.DataProviders
{
    public interface IProductProvider
    {
        IEnumerable<Product> GetProducts();
    }
}