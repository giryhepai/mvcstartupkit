﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.DataServices.DataProviders;
using WebApplication1.DataServices.Products;

namespace WebApplication1.DataServices.FakeDataServiceProviders
{
    public class FakeProductProvider : IProductProvider
    {
        public FakeProductProvider()
        {

        }

        public IEnumerable<Product> GetProducts()
        {
            const int numberOfFakeItems = 5;

            return GenerateFakeProducts(numberOfFakeItems);
        }

        private IEnumerable<Product> GenerateFakeProducts(int numberOfFakeItems)
        {
            IList<Product> products = new List<Product>();

            for (int i = 0; i < numberOfFakeItems; i++)
            {
                products.Add(new Product { Id = i + 1, Name = $"MyProduct_{i}", Price = 30 });
            }

            return products;
        }
    }
}