﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using WebApplication1.DataServices.DataProviders;
using WebApplication1.DataServices.Products;

namespace WebApplication1.DataServices.Tests.Products
{
    [TestClass]
    public class ProductsServiceTests
    {
        private Mock<IProductProvider> productProvider;

        private IProductsService service;

        private IEnumerable<Product> mockedProducts = new List<Product>()
        {
            new Product{ Id = 1, Name = "Product 1", Price = 30d },
            new Product{ Id = 2, Name = "Product 2", Price = 30d },
            new Product{ Id = 3, Name = "Product 3", Price = 30d },
        };

        [TestInitialize]
        public void TestInitialize()
        {
            this.productProvider = new Mock<IProductProvider>();

            this.service = GetService();
        }

        [TestMethod]
        public void GetProductById_IdExists_CorrectProductReturned()
        {
            //Arrange
            const int expectedProductId = 1;
            SetupProductProvider(mockedProducts);

            //Act
            Product result = service.GetProductById(expectedProductId);

            //Assert
            Assert.AreEqual(expectedProductId, result.Id);
        }

        [TestMethod]
        public void GetProductById_IdDoesNotExist_NullObjectReturned()
        {
            //Arrange
            const int expectedProductId = -1;
            SetupProductProvider(mockedProducts);

            //Act
            Product result = service.GetProductById(expectedProductId);

            //Assert
            Assert.IsNull(result);
        }

        private IProductsService GetService()
        {
            return new ProductsService(productProvider.Object);
        }

        private void SetupProductProvider(IEnumerable<Product> products)
        {
            productProvider.Setup(s => s.GetProducts())
                .Returns(products);
        }
    }
}
