﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.DataServices.Products;

namespace WebApplication1.Models
{
    public class ProductsModel
    {
        public IEnumerable<Product> Products { get; set; }
    }
}