﻿using System.Collections.Generic;
using System.Web.Mvc;
using WebApplication1.DataServices.Products;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class ProductsController : Controller
    {
        private readonly IProductsService productsService;

        public ProductsController(IProductsService productsService)
        {
            this.productsService = productsService;
        }

        public ActionResult Index()
        {
            IEnumerable<Product> allProducts = productsService.GetAllProducts();
            ProductsModel model = new ProductsModel() { Products = allProducts };

            return View(model);
        }
    }
}